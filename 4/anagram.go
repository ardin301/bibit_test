package main

import "fmt"

func main() {
	strs := []string{"kita", "atik", "tika", "aku", "kia", "makan", "kua"}
	strsAn := SplitByAnagram(strs)

	for _, val := range strsAn {
		fmt.Println(val)
	}
}

func SplitByAnagram(strs []string) [][]string {
	var retval [][]string
	retval = append(retval, []string{strs[0]})

	for i := 1; i < len(strs); i++ {
		var isInput bool
		for j := 0; j < len(retval); j++ {
			if CheckAnagram(retval[j][0], strs[i]) {
				isInput = true
				retval[j] = append(retval[j], strs[i])
				break
			}
		}
		if !isInput {
			retval = append(retval, []string{strs[i]})
		}
	}

	return retval
}

func CheckAnagram(str string, str2 string) bool {

	strLen := len(str)
	if len(str) != len(str2) {
		return false
	}
	alp := make(map[byte]int)
	for i := 0; i < strLen; i++ {
		alp[str[i]]++
		alp[str2[i]]--
	}

	for _, key := range alp {
		if key != 0 {
			return false
		}
	}

	return true
}
