package main

import (
	"testing"
)

func TestCheckAnagram(t *testing.T) {
	type field struct {
		word1 string
		word2 string
	}

	tests := []struct {
		name  string
		field field
		res   bool
	}{
		{
			"Testcase 1: is anagram",
			field{"neilarmstrong", "gnortsmralien"},
			true,
		},
		{
			"Testcase 2: is not anagram (different length)",
			field{"neilarmstrong", "gnortsmraliena"},
			false,
		},
		{
			"Testcase 3: is not anagram",
			field{"neilarmstrong", "gnortsmraliee"},
			false,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			if CheckAnagram(test.field.word1, test.field.word2) != test.res {
				t.Error("word", test.field.word1, "and word 2 :", test.field.word2, "should be", test.res)
			}
		})
	}
}
