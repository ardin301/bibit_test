package logs

import (
	context "context"
	"fmt"
	"time"

	grpc "google.golang.org/grpc"
)

const (
	address = "localhost:50052"
)

func InsertLog(message string, action string) error {
	// Set up a connection to the server.
	conn, err := grpc.Dial(address, grpc.WithInsecure(), grpc.WithBlock())
	if err != nil {
		fmt.Printf("did not connect: %v", err)
	}
	defer conn.Close()
	c := NewLogsClient(conn)

	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	_, err = c.InsertLog(ctx, &Log{Message: message, Action: action})
	return err
}
