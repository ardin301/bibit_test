package movies

import (
	context "context"
	"fmt"
	"io"
	"time"

	"google.golang.org/grpc"
)

const (
	address = "localhost:50051"
)

func GetByID(id string) (MovieData, error) {
	// Set up a connection to the server.
	conn, err := grpc.Dial(address, grpc.WithInsecure(), grpc.WithBlock())
	if err != nil {
		fmt.Printf("did not connect: %v", err)
	}
	defer conn.Close()
	c := NewMoviesClient(conn)

	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	r, err := c.GetByID(ctx, &MovieID{ID: id})
	if err != nil {
		return MovieData{}, err
	}

	retval := MovieData{
		ImdbId: r.ImdbId,
		Title:  r.Title,
		Year:   r.Year,
		Poster: r.Poster,
	}
	return retval, nil
}

func GetPerPage(s string, page int) ([]MovieData, error) {
	// Set up a connection to the server.
	conn, err := grpc.Dial(address, grpc.WithInsecure(), grpc.WithBlock())
	if err != nil {
		fmt.Printf("did not connect: %v", err)
	}
	defer conn.Close()
	c := NewMoviesClient(conn)

	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	r, err := c.GetPerPage(ctx, &MoviePage{S: s, Page: int32(page)})
	if err != nil {
		return make([]MovieData, 0), err
	}

	var movies []MovieData
	for {
		movie, err := r.Recv()
		if err == io.EOF {
			break
		}

		if err != nil {
			return make([]MovieData, 0), err
		}

		movies = append(movies, MovieData{
			ImdbId: movie.ImdbId,
			Title:  movie.Title,
			Year:   movie.Year,
			Poster: movie.Poster,
		})
	}

	return movies, nil
}
