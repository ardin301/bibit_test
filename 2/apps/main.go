package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"strings"

	"github.com/gorilla/mux"

	lh "movie-application/log_helper"
	mh "movie-application/movie_helper"
)

func main() {
	r := mux.NewRouter().StrictSlash(true)
	r.HandleFunc("/page/{page}/search/{s}", GetByPage)
	r.HandleFunc("/id/{id}", GetByID)
	fmt.Println("Now Listening at http://localhost:7071")
	log.Fatal(http.ListenAndServe(":7071", r))
}

func GetByID(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	key := vars["id"]

	resp, err := mh.GetByID(key)
	if err != nil {
		if strings.HasSuffix(err.Error(), "not found") {
			w.WriteHeader(http.StatusNotFound)
		} else {
			w.WriteHeader(http.StatusBadRequest)
			json.NewEncoder(w).Encode(err.Error())
		}
	} else {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		json.NewEncoder(w).Encode(resp)
	}
}

func GetByPage(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	search := vars["s"]
	page, err := strconv.Atoi(vars["page"])
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode("page should be a number")
	}
	go lh.InsertLog("Get Page "+vars["page"]+" Search "+search, "getpage")
	resp, err := mh.GetPerPage(search, page)
	if err != nil {
		if strings.HasSuffix(err.Error(), "not found") {
			w.WriteHeader(http.StatusNotFound)
		} else {
			w.WriteHeader(http.StatusBadRequest)
			json.NewEncoder(w).Encode(err.Error())
		}
	} else {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		json.NewEncoder(w).Encode(resp)
	}
}
