package movies

import (
	"testing"
)

func TestInitialize(t *testing.T) {
	type field struct {
		url    string
		apiKey string
	}

	tests := []struct {
		name  string
		field field
		resp  bool
	}{
		{
			"Testcase 1: valid url",
			field{"http://www.omdbapi.com/", "faf7e5bb"},
			true,
		},

		{
			"Testcase 2: invalid url",
			field{"api", "faf7e5bb"},
			false,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			_, err := Initialize(test.field.url, test.field.apiKey)
			//workaround to check if should be error or not
			if (err == nil && !test.resp) || (err != nil && test.resp) {
				t.Error("fail with url : ", test.field.url, "should be", test.resp, "got error", err)
			}
		})
	}
}

func TestGetByQueryParamNoRoute(t *testing.T) {
	type subresp struct {
		ImdbId string `json:"imdbID"`
	}

	type resp struct {
		Search       []subresp `json:"Search"`
		TotalResults string    `json:"totalResults"`
	}

	type field struct {
		ho    httpObj
		param map[string]string
	}

	tests := []struct {
		name  string
		field field
		resp  bool
	}{
		{
			"Testcase 1: valid request",
			field{
				httpObj{"https://api.github.com/repos/octocat/Hello-World", ""},
				map[string]string{},
			},
			true,
		},
		{
			"Testcase 2: invalid request 404",
			field{
				httpObj{"https://api.github.com/repos/octocat/Hello-Worldah", ""},
				map[string]string{},
			},
			false,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			var subFieldObj resp
			err := test.field.ho.GetByQueryParamNoRoute(test.field.param, &subFieldObj)
			if (err == nil && !test.resp) || (err != nil && test.resp) {
				t.Error("got error : ", err)
			}
		})
	}
}
