package movies

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
	"net/url"
)

type httpObj struct {
	BaseUrl string
	ApiKey  string
}

func Initialize(baseUrl string, apiKey string) (httpObj, error) {
	//validate the url if its valid
	if _, err := url.ParseRequestURI(baseUrl); err != nil {
		return httpObj{}, err
	}

	ho := httpObj{
		BaseUrl: baseUrl,
		ApiKey:  apiKey,
	}
	return ho, nil
}

func (ho httpObj) GetByQueryParamNoRoute(param map[string]string, v interface{}) error {
	u, err := url.Parse(ho.BaseUrl)
	if err != nil {
		return err
	}

	//append the apikey and all the necessary query param to the endpoint
	q := make(url.Values)
	q.Add("apikey", ho.ApiKey)
	for key, val := range param {
		q.Add(key, val)
	}
	u.RawQuery = q.Encode()

	//initialize the request
	req, err := http.NewRequest("GET", u.String(), nil)
	if err != nil {
		return err
	}

	//call the endpoint
	var client http.Client
	resp, err := client.Do(req)
	if err != nil {
		return err
	}

	//check if the response status code is 2xx
	if resp.StatusCode >= 200 && resp.StatusCode <= 299 {
		//cast the response to caller model
		bo, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return err
		}
		json.Unmarshal(bo, v)
		return nil
	} else {
		if resp.StatusCode == 404 {
			return errors.New("not found")
		} else if resp.StatusCode == 400 {
			return errors.New("bad request")
		}
		return errors.New("unknown error")
	}
}
