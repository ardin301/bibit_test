package movies

import "testing"

func TestGetMoviesBySearchPage(t *testing.T) {
	type field struct {
		s    string
		page int
	}

	tests := []struct {
		name  string
		field field
		resp  bool
	}{
		{
			"Testcase 1: get page exist",
			field{"Batman", 1},
			true,
		},
		{
			"Testcase 2: get page out of bound",
			field{"Batman", 1000},
			false,
		},
		{
			"Testcase 3: get page search query doesnt exist",
			field{"thissearchdoesntexist", 1},
			false,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			_, err := GetMoviesBySearchPage(test.field.s, test.field.page)
			if (err == nil && !test.resp) || (err != nil && test.resp) {
				t.Error("got error : ", err)
			}
		})
	}
}

func TestGetMovieByID(t *testing.T) {
	tests := []struct {
		name  string
		field string
		resp  bool
	}{
		{
			"Testcase 1: get by id found",
			"tt0372784",
			true,
		},
		{
			"Testcase 2: get by id not found",
			"thisiddoesntexist",
			false,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			_, err := GetMovieByID(test.field)
			if (err == nil && !test.resp) || (err != nil && test.resp) {
				t.Error("got error : ", err)
			}
		})
	}
}
