package movies

import (
	"errors"
	"strconv"

	dal "movie-service/dal"

	hh "movie-service/helper"
)

const (
	baseUrl = "http://www.omdbapi.com/"
	apiKey  = "faf7e5bb"
)

func GetMoviesBySearchPage(s string, page int) ([]dal.Movie, error) {
	//make the parameter compatible with http helper
	param := make(map[string]string)
	param["s"] = s
	param["page"] = strconv.Itoa(page)

	//initialize the response dto
	type response struct {
		Search       []dal.Movie `json:"Search"`
		TotalResults string      `json:"totalResults"`
	}
	var resp response

	//initialize the http helper
	ho, err := hh.Initialize(baseUrl, apiKey)
	if err != nil {
		return resp.Search, err
	}

	//call the endpoint
	err = ho.GetByQueryParamNoRoute(param, &resp)
	if err != nil {
		return resp.Search, err
	}

	if len(resp.Search) < 1 {
		return resp.Search, errors.New("not found")
	}

	//return the item in the page
	return resp.Search, nil
}

func GetMovieByID(i string) (dal.Movie, error) {
	//make the parameter compatible with http helper
	param := make(map[string]string)
	param["i"] = i

	//initialize the reponse dto
	var movie dal.Movie

	//initialize the http helper
	ho, err := hh.Initialize(baseUrl, apiKey)
	if err != nil {
		return movie, err
	}

	//call the endpoint
	err = ho.GetByQueryParamNoRoute(param, &movie)
	if err != nil {
		return movie, err
	}

	if (dal.Movie{}) == movie {
		return movie, errors.New("not found")
	}

	return movie, nil
}
