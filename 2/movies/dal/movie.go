package movies

type Movie struct {
	ImdbId string `json:"imdbID"`
	Title  string `json:"title"`
	Year   string `json:"year"`
	Poster string `json:"poster"`
}
