package main

import (
	"context"
	"fmt"
	"log"
	"net"

	mm "movie-service/bll"

	pb "movie-service/interface"

	"google.golang.org/grpc"
)

type server struct {
	pb.UnimplementedMoviesServer
}

const (
	port = ":50051"
)

func (s *server) GetByID(ctx context.Context, movieID *pb.MovieID) (*pb.Movie, error) {
	movie, err := mm.GetMovieByID(movieID.ID)
	fmt.Println(err)
	if err != nil {
		return &pb.Movie{}, err
	}

	movieDTO := pb.Movie{
		ImdbId: movie.ImdbId,
		Title:  movie.Title,
		Year:   movie.Year,
		Poster: movie.Poster,
	}

	return &movieDTO, nil
}

func (s *server) GetPerPage(in *pb.MoviePage, gpps pb.Movies_GetPerPageServer) error {
	movies, err := mm.GetMoviesBySearchPage(in.S, int(in.Page))
	if err != nil {
		return err
	}
	for _, movie := range movies {
		movieDTO := pb.Movie{
			ImdbId: movie.ImdbId,
			Title:  movie.Title,
			Year:   movie.Year,
			Poster: movie.Poster,
		}
		if err := gpps.Send(&movieDTO); err != nil {
			return err
		}
	}
	return nil
}

func main() {
	lis, err := net.Listen("tcp", port)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	s := grpc.NewServer()
	pb.RegisterMoviesServer(s, &server{})
	log.Printf("server listening at %v", lis.Addr())
	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}
