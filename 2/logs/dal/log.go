package logs

import "time"

type Log struct {
	Message string    `json:"userId"`
	Action  string    `json:"action"`
	Date    time.Time `json:"date"`
}
