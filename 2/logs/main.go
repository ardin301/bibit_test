package main

import (
	"context"
	"log"
	lm "log-service/bll"
	pb "log-service/interface"
	"net"

	"google.golang.org/grpc"
)

type server struct {
	pb.UnimplementedLogsServer
}

const (
	port = ":50052"
)

func (s *server) InsertLog(ctx context.Context, in *pb.Log) (*pb.LogResponse, error) {
	err := lm.InsertLog(in.Message, in.Action)
	if err != nil {
		return &pb.LogResponse{Response: "failed to insert log"}, err
	}

	return &pb.LogResponse{Response: "succeed to insert log"}, nil
}

func main() {
	lis, err := net.Listen("tcp", port)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	s := grpc.NewServer()
	pb.RegisterLogsServer(s, &server{})
	log.Printf("server listening at %v", lis.Addr())
	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}
