package logs

import "testing"

func TestInsert(t *testing.T) {
	tests := []struct {
		name  string
		field string
		resp  bool
	}{
		{
			"Testcase 1: insert item",
			"new data",
			true,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			err := Insert(test.field)
			if (err == nil && !test.resp) || (err != nil && test.resp) {
				t.Error("got error : ", err)
			}
		})
	}
}
