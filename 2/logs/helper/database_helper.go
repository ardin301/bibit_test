package logs

import "fmt"

func Insert(v interface{}) error {
	fmt.Println("inserted", v, "into the database")
	return nil
}
