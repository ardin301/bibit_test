package logs

import "testing"

func TestInsertLog(t *testing.T) {
	type field struct {
		message string
		action  string
	}

	tests := []struct {
		name  string
		field field
		resp  bool
	}{
		{
			"Testcase 1: insert log",
			field{"Get Page 1 Search Batman", "getpage"},
			true,
		},

		{
			"Testcase 2: failed log too long",
			field{
				"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
				"getpage",
			},
			false,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			err := InsertLog(test.field.message, test.field.action)
			if (err == nil && !test.resp) || (err != nil && test.resp) {
				t.Error("got error : ", err)
			}
		})
	}
}
