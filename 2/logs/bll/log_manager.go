package logs

import (
	"errors"
	"time"

	dal "log-service/dal"
	dh "log-service/helper"
)

func InsertLog(message string, action string) error {
	if len(message) > 50 {
		return errors.New("message too long")
	}

	l := dal.Log{
		Message: message,
		Date:    time.Now().UTC(),
		Action:  action,
	}
	err := dh.Insert(l)
	return err
}
