package main

import (
	"testing"
)

func TestFindFirstStringInBracketV2(t *testing.T) {
	tests := []struct {
		name  string
		field string
	}{
		{
			"Testcase 1: one bracket",
			"testcase (one) word",
		},
		{
			"Testcase 2: no bracket",
			"testcase one word",
		},
		{
			"Testcase 3: false order of bracket",
			"testcase )one( word",
		},
		{
			"Testcase 4: multiple bracket",
			"testcase (one) (word)",
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ori := findFirstStringInBracket(test.field)
			ref := findFirstStringInBracketV2(test.field)
			if ori != ref {
				t.Error("input word", test.field, "original got :", ori, ", refactor got :", ref)
			}
		})
	}
}
