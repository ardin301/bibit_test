package main

import (
	"fmt"
	"strings"
)

func main() {
	str := "asdasd(asdasd123)"
	fmt.Println(findFirstStringInBracket(str))
	fmt.Println(findFirstStringInBracketV2(str))
}

func findFirstStringInBracket(str string) string {
	if len(str) > 0 {
		indexFirstBracketFound := strings.Index(str, "(")
		if indexFirstBracketFound >= 0 {
			runes := []rune(str)
			wordsAfterFirstBracket := string(runes[indexFirstBracketFound:len(str)])
			indexClosingBracketFound := strings.Index(wordsAfterFirstBracket, ")")
			if indexClosingBracketFound >= 0 {
				runes := []rune(wordsAfterFirstBracket)
				return string(runes[1 : indexClosingBracketFound-1])
			} else {
				return ""
			}
		} else {
			return ""
		}
	} else {
		return ""
	}
	return ""
}

func findFirstStringInBracketV2(str string) string {
	if len(str) == 0 {
		return ""
	}

	indexFirstBracketFound := strings.Index(str, "(")
	if indexFirstBracketFound < 0 {
		return ""
	}
	indexClosingBracketFound := strings.Index(str[indexFirstBracketFound+1:], ")")
	if indexClosingBracketFound < 0 {
		return ""
	}

	return str[indexFirstBracketFound+1 : indexClosingBracketFound+indexFirstBracketFound]
}
